import Realm from 'realm';
import { FilterSchema, SubtaskSchema, TaskSchema } from '../schemas/TaskSchema';

export function getRealmApp() {
  const appId = 'skoolrealmdb-lvuzo';
  const appConfig = {
    id: appId,
    timeout: 10000,
    app: {
      name: 'default',
      version: '0',
    },
  };
  return new Realm.App(appConfig);
}

export function getRealm() {
  const realmApp = getRealmApp();
  return Realm.open({
    schema: [FilterSchema, SubtaskSchema, TaskSchema],
    schemaVersion: 1,
    sync: {
      user: realmApp.currentUser,
      partitionValue: realmApp.currentUser ? realmApp.currentUser.id : 'unknownUser'
    },
  });
}
